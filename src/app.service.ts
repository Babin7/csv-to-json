import { Injectable } from '@nestjs/common';
import * as fs from 'fs'
import * as _ from 'lodash'
import { IRow } from './app.interface'

@Injectable()
export class AppService {
  filePath: string = './test.csv'

  getHello(): string {
    return 'Hello World!';
  }

  // this function return array of key-value pair or IRow interface (key = "column name" and value="value").
  extractDataFromCSVFile(): IRow[]{
    let data: any = fs.readFileSync(this.filePath, { encoding: 'utf-8' });
    data = data.split('\n').map(row => row.split(','));
    data = data.map(row => _.dropRightWhile(row, val => val === ''));
    // assiging columns value and removing columns name from data.
    // assiging new column name for percentage value...

    data[0][data[0].length - 1] = "Percent"
    const columns = data[0];
    data.shift();

    // row.length = 18 (reduced)
    // column.length = 30 (will be 29? after reducing)
    // array of [rows]

    const rows: IRow[] = []
    for (let i = 0; i < 17; i++){
      const row = data[i]
      const individual_row_item: IRow = {
        // below code row[1] is not in SOLID architecture...
        id: i,
        name: row[0],
        email: row[1],
        bed: row[2],
        bew: row[3],
        boek: row[4],
        book: row[5],
        buurt: row[6],
        dar:row[7],
        do: row[8],
        ivers: row[9],
        mon: row[10],
        ohh: row[11],
        osre: row[12],
        ppa: row[13],
        rbb: row[14],
        sc: row[15],
        sit: row[16],
        zsg: row[17],
        ds: row[18],
        ta: row[19],
        ts: row[20],
        codap: row[21],
        seev: row[22],
        sublet: row[23],
        tesla: row[24],
        other: row[25],
        sales: row[26],
        total: row[27],
        available: row[28],
        percent: row[29]
      }

      rows.push(individual_row_item)
    }


    return rows
  }
}
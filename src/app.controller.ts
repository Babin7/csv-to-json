import { Controller, Delete, Get, Param } from '@nestjs/common';
import { IRow } from './app.interface';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    this.appService.extractDataFromCSVFile();
    return this.appService.getHello();
  }


  @Get('/name')
  getAllUserName(): any[]{
    const rows = this.appService.extractDataFromCSVFile()
    const username: any[] = [];

    rows.map((val: IRow) => username.push(
      {
      id: val.id,
      username: val.name
      }
    ))


    return username
  }

  @Get('/name/:id')
  getUserByID(@Param('id') uID: string): IRow{
    const rows = this.appService.extractDataFromCSVFile();
    const id_val = rows.filter((val: IRow) => val.id === JSON.parse(uID))

    return id_val[0];
  }
}